package main;

import java.util.Date;

/**
 * Цены товара
 */
public class PriceImpl implements Price {
    private long id; // идентификатор в БД
    private String product_code; // код товара
    private int number; // номер цены
    private int depart; // номер отдела
    private Date begin; // начало действия
    private Date end; // конец действия
    private long value; // значение цены в копейках

    public PriceImpl(long id, String product_code, int number, int depart, Date begin, Date end, long value) {
        this.id = id;
        this.product_code = product_code;
        this.number = number;
        this.depart = depart;
        this.begin = begin;
        this.end = end;
        this.value = value;
    }

    @Override
    public String getProduct_code() {
        return product_code;
    }

    @Override
    public int getNumber() {
        return number;
    }

    @Override
    public int getDepart() {
        return depart;
    }

    @Override
    public Date getBegin() {
        return begin;
    }

    @Override
    public Date getEnd() {
        return end;
    }

    @Override
    public long getValue() {
        return value;
    }

    @Override
    public void setBegin(Date begin) {
        this.begin = begin;
    }

    @Override
    public void setEnd(Date end) {
        this.end = end;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public Price cloneWithDefaultId() {
        return new PriceImpl(Long.MIN_VALUE,this.getProduct_code(),this.getNumber(),this.getDepart(),this.getBegin(),this.getEnd(),this.getValue());
    }

    @Override
    public String toString() {
        return this.getId()+
                " " + this.getProduct_code() +
                " " + this.getNumber() +
                " " + this.getDepart()+
                " " + this.getBegin() +
                " " + this.getEnd() +
                " " + this.getValue();
    }
}
