package main;

import java.util.Date;

/**
 * Интерфейс для цены товара
 */
public interface Price {
    String getProduct_code();

    int getNumber();

    int getDepart();

    Date getBegin();

    Date getEnd();

    long getValue();

    void setBegin(Date begin);

    void setEnd(Date end);

    void setId(long id);


    long getId();

    Price cloneWithDefaultId();

}
