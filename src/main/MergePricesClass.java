package main;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Класс для расположения метода по объеденению строк.
 */
public class MergePricesClass {
    private Collection<Price> pricesInDB;
    private Collection<Price> pricesToMerge;

    public Collection<Price> mergePrices(Collection<Price> oldPrices, Collection<Price> newPrices){
        for (Price newPrice :
                newPrices) {
            List<Price> oldCrossingPrices = oldPrices.stream().filter(price -> hasSameAttrs(newPrice, price) && crossInTime(newPrice, price)).sorted(Comparator.comparing(Price::getBegin)).collect(Collectors.toList());
            if (oldCrossingPrices.size() > 0){
                mergeCrossingPrices(newPrice,oldCrossingPrices, oldPrices);
            }
        }
        return oldPrices;
    }

    private boolean crossInTime(Price firstPrice, Price secondPrice) {
        return firstPrice.getBegin().before(secondPrice.getEnd())&&secondPrice.getBegin().before(firstPrice.getEnd());
    }

    private boolean hasSameAttrs(Price newPrice, Price price) {
        return price.getProduct_code().equals(newPrice.getProduct_code()) && price.getNumber() == newPrice.getNumber() && price.getDepart() == newPrice.getDepart();
    }

    private void mergeCrossingPrices(Price newPrice, List<Price> crossingPrices, Collection<Price> resultPrices){
        resultPrices.removeAll(crossingPrices);
        resultPrices.add(newPrice);
        // обрабатываем начальный конец исходных пересекающихся точек.
        Price headPrice = crossingPrices.get(0);
        Price tailPrice = crossingPrices.get(crossingPrices.size()-1);
        if (headPrice == tailPrice) tailPrice = tailPrice.cloneWithDefaultId();
        if (headPrice.getBegin().before(newPrice.getBegin())){
            if (headPrice.getValue() == newPrice.getValue()){
                newPrice.setBegin(headPrice.getBegin());
            } else {
                headPrice.setEnd(newPrice.getBegin());
                resultPrices.add(headPrice);
            }
        }
        // обрабатываем конец исходных пересекающихся точек.
        if (tailPrice.getEnd().after(newPrice.getEnd())){
            if (tailPrice.getValue() == newPrice.getValue()){
                newPrice.setEnd(tailPrice.getEnd());
            } else {
                tailPrice.setBegin(newPrice.getEnd());
                resultPrices.add(tailPrice);
            }
        }

    }


    Collection<Price> initDefaultPricesInDB() throws ParseException {
        Collection<Price> prices = new ArrayList<>();
        prices.add(new PriceImpl(1L,"122856",1,1, parseDateFromString("01.01.2013 00:00:00"), parseDateFromString("31.01.2013 23:59:59"),11000));
        prices.add(new PriceImpl(2L,"122856",2,1, parseDateFromString("10.01.2013 00:00:00"), parseDateFromString("20.01.2013 23:59:59"),99000));
        prices.add(new PriceImpl(3L,"6654",1,2, parseDateFromString("01.01.2013 00:00:00"), parseDateFromString("31.01.2013 00:00:00"),5000));

        return prices;
    }

    Collection<Price> initDefaultPricesToMerge() throws ParseException {
        Collection<Price> prices = new ArrayList<>();
        prices.add(new PriceImpl(4L,"122856",1,1, parseDateFromString("20.01.2013 00:00:00"), parseDateFromString("20.02.2013 23:59:59"),11000));
        prices.add(new PriceImpl(5L,"122856",2,1, parseDateFromString("15.01.2013 00:00:00"), parseDateFromString("25.01.2013 23:59:59"),92000));
        prices.add(new PriceImpl(6L,"6654",1,2, parseDateFromString("12.01.2013 00:00:00"), parseDateFromString("13.01.2013 00:00:00"),4000));

        return prices;
    }


    private Date parseDateFromString(String stringDate) throws ParseException {
        DateFormat format = new SimpleDateFormat("dd.MM.yyyy kk:mm:ss", Locale.ENGLISH);
        return format.parse(stringDate);
    }

    public static void main(String[] args) throws ParseException {
        MergePricesClass mergePricesClass = new MergePricesClass();
        mergePricesClass.pricesInDB = mergePricesClass.initDefaultPricesInDB();
        mergePricesClass.pricesToMerge = mergePricesClass.initDefaultPricesToMerge();
        Collection<Price> resultPrices = mergePricesClass.mergePrices(mergePricesClass.pricesInDB, mergePricesClass.pricesToMerge);
        for (Price price: resultPrices
             ) {
            System.out.println(price);
        }

    }
}
